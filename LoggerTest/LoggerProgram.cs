﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLibrary;

namespace LoggerTest
{
    class LoggerProgram
    {
        /// <summary>
        /// Test method!
        /// It calls the LogWriter call, witch contains by default the three loggers
        /// </summary>
        /// <remarks><see cref="LogWriter.cs"/></remarks>
        static void Main()
        {
            LogWriter log = new LogWriter();

            for (int i = 0; i < 100; i++)
            {
                log.Log((i % 3) + 1, "Log Message " + i);
            }

            Console.ReadLine();
        }
    }
}
