﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLibrary.Abstract; 

namespace LoggerLibrary
{
    /// <summary>
    /// The class contains several loggers.
    /// With his "log" method it logs on all the containing loggers.
    /// All the loggings are asynced, so it may swicth the order!
    /// All the loggers are called with default settings!
    /// </summary>
    public class LogWriter
    {
        public List<IMyLogger> Loggers { get; set; }

        public LogWriter(IEnumerable<IMyLogger> loggers)
        {
            this.Loggers = loggers.ToList();
        }

        /// <summary>
        /// The default contructor.
        /// It puts three of the loggers.
        /// All the logger are call with their default settings!
        /// </summary>
        public LogWriter()
        {
            this.Loggers = new List<IMyLogger>();
            this.Loggers.Add(new ConsoleLogger());
            this.Loggers.Add(new FileLogger());
            this.Loggers.Add(new HTTPLogger());
        }

        public void AddLogger(IMyLogger logger)
        {
            this.Loggers.Add(logger);
        }

        public void AddLoggers(IEnumerable<IMyLogger> loggers)
        {
            this.Loggers.AddRange(loggers);
        }

        /// <summary>
        /// That log method calls every logger to log the same thing.
        /// It's called async so the order may be swapped.
        /// </summary>
        public void Log(int level, string message)
        {
            foreach (var logger in this.Loggers)
            {
				Task.Factory.StartNew(() =>
				{
					lock (logger)
					{
						logger.Log(level, message);
					}
				});
            }
        }
    }
}
