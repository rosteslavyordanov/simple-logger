﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerLibrary
{
    /// <summary>
    /// The Enum that represents the levels for the messages that are
    /// logger though all the logger classes
    /// </summary>
    public enum Level
    {
        INFO = 1,
        WARNING,
        PLSCHECKFFS
    }
}
