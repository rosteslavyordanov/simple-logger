﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerLibrary.Abstract
{
    /// <summary>
    /// Interface for the logger.
    /// All loggers should implement that
    /// </summary>
    public interface IMyLogger
    {
        /// <summary>
        /// The main log method
        /// </summary>
        /// <param name="level">Level of the log</param>
        /// <param name="message">Message to be logged</param>
        void Log(int level, string message);
    }
}
