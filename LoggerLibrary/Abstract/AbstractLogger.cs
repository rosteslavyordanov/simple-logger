﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerLibrary.Abstract
{
    /// <summary>
    /// Inherits the IMyLogger interface, giving 
    /// the parts that are the same for every logger
    /// </summary>
    public abstract class AbstractLogger : IMyLogger
    {
        /// <summary>
        /// Format of the log message
        /// </summary>
        protected const string MessageFormat = "{0}::{1}::{2}";
        /// <summary>
        /// The SO 8901 format for the date 
        /// </summary>
        protected const string DateFormat = "o";

        /// <summary>
        /// Represents the integer value as a real Level in the enum
        /// </summary>
        /// <param name="levelAsInt">The level as int may be from 1 to 3</param>
        /// <returns>Returns the level as item from the enum</returns>
        protected virtual Level ToEnum(int levelAsInt)
        {
            if (levelAsInt < 1 ||
                levelAsInt > 3)
            {
                throw new IndexOutOfRangeException("level index must be between 1 and 3 (includeing)");
            }
            Level level = (Level)levelAsInt;
            return level;
        }

        /// <summary>
        /// Message builder that returns the massage for all the loggers
        /// </summary>
        protected virtual string BuildMessage(Level level, string message)
        {
            return string.Format(
                MessageFormat,
                level,
                DateTime.Now.ToString(DateFormat),
                message);
        }

        /// <summary>
        /// That is tha abstract method that every logger should implements
        /// This is the only method that a logger can log though.
        /// </summary>
        public abstract void Log(int level, string message);
    }
}
