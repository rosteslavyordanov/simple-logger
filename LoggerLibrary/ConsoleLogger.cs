﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLibrary.Abstract;

namespace LoggerLibrary
{
    public class ConsoleLogger : AbstractLogger
    {
        /// <summary>
        /// The Log method for the console logger.
        /// It logs on the console
        /// </summary>
        public override void Log(int intLevel, string message)
        {
            Level level = base.ToEnum(intLevel);
            Console.WriteLine(
                base.BuildMessage(level, message)
                );
        }
    }
}
