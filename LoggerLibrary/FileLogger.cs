﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLibrary.Abstract;
using System.IO;

namespace LoggerLibrary
{
    public class FileLogger : AbstractLogger
    {
        /// <summary>
        /// That's the default path that will be used,
        /// if no other path is given for the class
        /// </summary>
        const string DefaultPath = @"C:\temp\Log.log";

        /// <summary>
        /// The path that will be userd to log there
        /// If no such path is told it will be the default
        /// </summary>
        public string Path { get; set; }

        public FileLogger(string path)
        {
            this.Path = path;
        }

        public FileLogger()
            : this(DefaultPath)
        { }

        /// <summary>
        /// The main logging method that logs for this class
        /// </summary>
        public override void Log(int levelAsInt, string message)
        {
            Level level = base.ToEnum(levelAsInt);
            string directoryPath = this.Path.Substring(0, this.Path.LastIndexOf('\\'));
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            if (!File.Exists(this.Path))
            {
                // Create a file to write to. 
                using (StreamWriter sw = File.CreateText(this.Path))
                {
                    sw.WriteLine(base.BuildMessage(
                        level,
                        message
                        ));
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(this.Path))
                {
                    sw.WriteLine(base.BuildMessage(
                        level,
                        message
                        ));
                }
            }
        }
    }
}
