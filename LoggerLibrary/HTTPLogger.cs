﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerLibrary.Abstract;
using System.Net;
using System.IO;

namespace LoggerLibrary
{
    public class HTTPLogger : AbstractLogger
    {
        /// <summary>
        /// That's the default domain that will be used,
        /// if no other domain is given for the class
        /// </summary>
        const string DefauldDomain = "http://localhost:8080";

        /// <summary>
        /// The domain that will be userd to log there
        /// If no such domain is told it will be the default
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// That is the Http requester that make the request to 
        /// make the POST
        /// </summary>
        public HttpWebRequest HttpWReq { get; set; }

        /// <summary>
        /// The encoding  that encodes the date
        /// </summary>
        public ASCIIEncoding Encoding { get; set; }

        public HTTPLogger(string domain)
        {
            this.Domain = domain;
            this.HttpWReq = (HttpWebRequest)WebRequest.Create(this.Domain);
            this.HttpWReq.Method = "POST";
            this.HttpWReq.ContentType = "text/html";
            this.Encoding = new ASCIIEncoding();
        }
        public HTTPLogger() 
            : this(DefauldDomain) 
        {
        }

        /// <summary>
        /// The main logging method that logs for this class
        /// </summary>
        public override void Log(int levelAsInt, string message)
        {
            Level level = base.ToEnum(levelAsInt);
            string postData = base.BuildMessage(level, message);
            byte[] data = this.Encoding.GetBytes(postData);
            this.HttpWReq.ContentLength = data.Length;

            using (Stream stream = HttpWReq.GetRequestStream())
            {
                stream.Write(data,0,data.Length);
            }
        }
    }
}
